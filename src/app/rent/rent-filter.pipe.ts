import { Pipe, PipeTransform } from "@angular/core";

import { Rent } from './rent';

@Pipe({
  name: 'rentFilter'
})
export class RentFilterPipe implements PipeTransform {

  transform(value: Rent[], filterByTitle: string, filterByBorrower: string): Rent[] {
    filterByTitle = filterByTitle ? filterByTitle.toLocaleLowerCase() : null;
    filterByBorrower = filterByBorrower ? filterByBorrower.toLocaleLowerCase() : null;
    
    if (filterByBorrower && filterByTitle) {
      return value.filter((rent: Rent) => (rent.borrower.toLocaleLowerCase().indexOf(filterByBorrower) !== -1 && rent.movieTitle.toLocaleLowerCase().indexOf(filterByTitle) !== -1));
    }
    if (!filterByBorrower && filterByTitle) {
      return value.filter((rent: Rent) => rent.movieTitle.toLocaleLowerCase().indexOf(filterByTitle) !== -1);
    }
    if (filterByBorrower && !filterByTitle) {
      return value.filter((rent: Rent) => rent.borrower.toLocaleLowerCase().indexOf(filterByBorrower) !== -1);
    }
    if (!filterByBorrower && !filterByTitle) {
      return value;
    }
  }
}
