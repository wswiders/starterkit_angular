import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';

import { RentComponent } from './rent.component';
import { ReturnMovieComponent } from './return-movie/return-movie.component';
import { MoviesService } from '../movies/movies.service';
import { RentService } from './rent.service';

import { RentFilterPipe } from './rent-filter.pipe';
import { RentSortPipe } from './rent-sort.pipe';

import { RatingModule } from "ngx-rating";

import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      { path: 'rents', component: RentComponent }
    ]),
    RatingModule
  ],
  declarations: [
    RentComponent,
    ReturnMovieComponent,
    RentFilterPipe,
    RentSortPipe],
  providers: [MoviesService, RentService]
})
export class RentModule { }
