import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA }    from '@angular/core';

import { RentFilterPipe } from './rent-filter.pipe';
import { RentSortPipe } from './rent-sort.pipe';

import { RentComponent } from './rent.component';
import { RentService } from './rent.service';
import {Rent} from './rent';

describe('Rent Component', () => {

  let comp: RentComponent;
  let fixture: ComponentFixture<RentComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  let expected = {
    "id": 0,
    "movieId": "1",
    "movieTitle": "Bułgarski pościkk",
    "price": 9.50,
    "borrower": "Mariusz Strzelba",
    "lendDate": "2017-03-02",
    "returnDate": "2017-03-15",
    "unrewindedTape": "",
    "daysCount": 13,
    "delay": 0,
    "brokenTape": "",
    "pay": "123.50"
  };

  beforeEach(async(() => {
    let mockRentService = {
      rents: [
        expected,
        {
          "id": 1,
          "movieId": "2",
          "movieTitle": "Sarnie żniwo, czyli pokusa statuetkowego szlaku",
          "price": 11.50,
          "borrower": "Andrzej Strzelba",
          "lendDate": "2017-03-11",
          "returnDate": "",
          "unrewindedTape": "",
          "delay": "",
          "brokenTape": "",
          "pay": "",
          "daysCount": 0
        }],
      fbGetListOfRentedMovie: () => true
    }
    TestBed.configureTestingModule({
      declarations: [RentComponent, RentFilterPipe, RentSortPipe],
      providers: [
        { provide: RentService, useValue: mockRentService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
    el = fixture.nativeElement;
  });

  describe('init display', () => {

    it('should set correct title', () => {
      // when
      comp.ngOnInit();
      fixture.detectChanges();
      // then
      expect(el.querySelector('.panel-heading').textContent).toContain('Rent Move List');
    });

    it('should set elements in table in default correct order', () => {
      //when
      comp.ngOnInit();
      fixture.detectChanges();
      //then
      expect(el.querySelector('[t1]').textContent).toContain("Sarnie żniwo, czyli pokusa statuetkowego szlaku");
    });
  });

  describe('display after modification', () => {

    it('should set elements in table in correct order ascending when orderBy is ID, ascending', () => {
      // given
      comp.orderBy = 'id';
      comp.asc = true;
      //when
      comp.ngOnInit();
      fixture.detectChanges();
      //then
      expect(el.querySelector('[t1]').textContent).toContain("Bułgarski pościkk");
    });

    it('should filter elements by movie title', () => {
      //given
      comp.movieTitleRentFilter = 'Sarnie'
      //when
      comp.ngOnInit();
      fixture.detectChanges();
      //then
      expect(el.querySelector('.table-responsive')).not.toContain("Bułgarski pościkk");
      expect(el.querySelector('[t1]').textContent).toContain("Sarnie żniwo, czyli pokusa statuetkowego szlaku");
    });

    it('should filter elements by borrower name', () => {
      //given
      comp.borrowerFilter = 'Andrzej'
      //when
      comp.ngOnInit();
      fixture.detectChanges();
      //then
      expect(el.querySelector('.table-responsive')).not.toContain("Bułgarski pościkk");
      expect(el.querySelector('[t1]').textContent).toContain("Sarnie żniwo, czyli pokusa statuetkowego szlaku");
    });
  });

  describe('method testing', () => {

    it('should set correct selectedRent and copiedRent', () => {
      //given
      let expectedRent: Rent = new Rent();
      expectedRent.id = 0;
      expectedRent.movieId = 1;
      expectedRent.borrower = "Mariusz Strzelba";
      expectedRent.price = 9.50;
      expectedRent.movieTitle = "Bułgarski pościkk";
      expectedRent.lendDate = "2017-03-02";
      expectedRent.returnDate = "2017-03-15";
      expectedRent.unrewindedTape = false;
      expectedRent.daysCount = 13;
      expectedRent.delay = 0;
      expectedRent.brokenTape = false;
      expectedRent.pay = 123.5;
      //when
      comp.selectRentedMovie(expectedRent);
      //then
      expect(comp.selectedRent.movieTitle).toContain('Bułgarski pościkk');
      expect(comp.copiedRent.movieTitle).toContain('Bułgarski pościkk');
    });

    it('should set correct sortParam and is asc param', () => {
      //when
      comp.setSortParams("movieTitle", false);
      //then
      expect(comp.orderBy).toContain('movieTitle');
      expect(comp.asc).toBe(false);
    });
  });

  describe('page event', () => {

    it('should call selectRentedMovie method when press rent button', () => {
      // given
      let spy = spyOn(comp, 'selectRentedMovie');
      comp.ngOnInit();
      fixture.detectChanges();
      de = de.query(By.css('.button'));
      el = de.nativeElement;
      //when
      el.click();
      // then
      expect(spy).toHaveBeenCalled();
    });

    it('should call setSortParams method when press button', () => {
      // given
      let spy = spyOn(comp, 'setSortParams');
      comp.ngOnInit();
      fixture.detectChanges();
      de = de.query(By.css('#titleAsc'));
      let buttonUp = de.nativeElement;
      //when
      buttonUp.click();
      fixture.detectChanges();
      // then
      expect(spy).toHaveBeenCalledWith('movieTitle', true);
    });
  });

});
