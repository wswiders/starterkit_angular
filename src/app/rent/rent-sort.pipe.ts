import { Pipe, PipeTransform } from '@angular/core';

import { Rent } from './rent';

@Pipe({
  name: 'sortRent'
})
export class RentSortPipe implements PipeTransform {

  transform(value: Rent[], sortedParam: string, asc: boolean): Rent[] {
    value.sort((a, b) => {
      if (a[sortedParam] < b[sortedParam]) {
        return -1;
      } else if (a[sortedParam] > b[sortedParam]) {
        return 1;
      } else {
        return 0;
      }
    });
    if (asc) {
      return value;
    } else {
      return value.reverse();
    }
  }
}
