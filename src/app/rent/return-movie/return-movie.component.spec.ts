import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA }    from '@angular/core';

import { SharedModule } from '../../shared/shared.module';

import { RentService } from '../rent.service';
import { MoviesService } from '../../movies/movies.service';
import { ReturnMovieComponent } from './return-movie.component';
import { Rent } from '../rent';

describe('Return Movie Component', () => {

  let comp: ReturnMovieComponent;
  let fixture: ComponentFixture<ReturnMovieComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    let mockRentService = {
      calculatePay: () => 0,
      fbGetListOfRentedMovie: () => true,
      fbReturnTape: () => true
    };
    let movkMoviesService = {
      fbChangeMovieStatusToFree: () => true,
      fbAddRating: () => true
    };
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ReturnMovieComponent],
      providers: [
        { provide: RentService, useValue: mockRentService },
        { provide: MoviesService, useValue: movkMoviesService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnMovieComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
    el = fixture.nativeElement;
  });

  describe('page display', () => {

    let inputBorrower: HTMLInputElement;
    let inputMovie: HTMLInputElement;
    let inputDate: HTMLInputElement;

    beforeEach(() => {
      comp.rent = new Rent();
      comp.rent.id = 0;
      comp.rent.movieId = 1;
      comp.rent.borrower = "Mariusz Strzelba";
      comp.rent.price = 9.50;
      comp.rent.movieTitle = "Bułgarski pościkk";
      comp.rent.lendDate = "2017-03-02";
    });

    it('should set correct data on modal window', async(() => {
      //when
      fixture.detectChanges();
      //then
      fixture.whenStable().then(() => {
        let deBorrower = de.query(By.css('#borrower'));
        inputBorrower = deBorrower.nativeElement;
        let deMovie = de.query(By.css('#movie'));
        inputMovie = deMovie.nativeElement;
        let deDate = de.query(By.css('#returnDate'));
        inputDate = deDate.nativeElement;
        expect(inputBorrower.value).toContain('Mariusz Strzelba');
        expect(inputMovie.value).toContain('Bułgarski pościkk');
        expect(inputDate.value).toBeNull;
      });
    }));

    it('should set correct date on modal window when change it in object', async(() => {
      //given
      comp.rent.returnDate = "2017-04-02"
      //when
      fixture.detectChanges();
      //then
      fixture.whenStable().then(() => {
        let deBorrower = de.query(By.css('#borrower'));
        inputBorrower = deBorrower.nativeElement;
        let deMovie = de.query(By.css('#movie'));
        inputMovie = deMovie.nativeElement;
        let deDate = de.query(By.css('#returnDate'));
        inputDate = deDate.nativeElement;
        expect(inputBorrower.value).toContain('Mariusz Strzelba');
        expect(inputMovie.value).toContain('Bułgarski pościkk');
        expect(inputDate.value).toContain('2017-04-02');
      });
    }));

    it('should set 0 on total amount label', async(() => {
      //when
      fixture.detectChanges();
      //then
      fixture.whenStable().then(() => {
        de = de.query(By.css('.payment'));
        expect(de.nativeElement.textContent).toContain("Total Amount: $0.00");
      });
    }));
  });

  describe('page events', () => {

    beforeEach(() => {
      comp.rent = new Rent();
      comp.rent.id = 0;
      comp.rent.movieId = 1;
      comp.rent.borrower = "Mariusz Strzelba";
      comp.rent.price = 9.50;
      comp.rent.movieTitle = "Bułgarski pościkk";
      comp.rent.lendDate = "2017-03-02";
    });

    it('should set unrewindedTape value to true when click on checkbox', async(() => {
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        //given
        let spy = spyOn(comp, 'calculateAmount');
        de = de.query(By.css('#unrewindedTape'));
        el = de.nativeElement;
        //when
        el.click();
        //then
        expect(comp.rent.unrewindedTape).toBe(true);
      });
    }));

    it('should call returnMovie method with correct param when click on returnBtn', async(() => {
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        //given
        let spyReturnMovie = spyOn(comp, 'returnMovie');
        de = de.query(By.css('#returnMovieBtn'));
        el = de.nativeElement;
        //when
        el.click();
        //then
        expect(spyReturnMovie).toHaveBeenCalledWith(comp.rent);
      });
    }));

    it('should movie button be disabled when required inputs is invalid', async(() => {
      //set
      let button: HTMLInputElement;
      let returnDate: HTMLInputElement;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        //given
        let deDate = de.query(By.css('#returnDate'));
        returnDate = deDate.nativeElement;
        //when
        returnDate.value = '';
        fixture.detectChanges();
        //then
        let deButton = de.query(By.css('#returnMovieBtn'));
        button = deButton.nativeElement;
        expect(button.disabled).toBe(true);
      });
    }));

    it('should movie button be available when required inputs is valid', async(() => {
      //given
      comp.rent.returnDate = "2017-04-04";
      let button: HTMLInputElement;
      //when
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        //then
        let deButton = de.query(By.css('#returnMovieBtn'));
        button = deButton.nativeElement;
        expect(button.disabled).toBe(false);
      });
    }));
  });

});
