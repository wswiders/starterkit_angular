import {Component, Input} from '@angular/core';

import {Rent} from '../../rent/rent';

import { MoviesService } from '../../movies/movies.service';
import { RentService } from '../rent.service';

@Component({
  moduleId: module.id,
  selector: 'return-movie',
  templateUrl: 'return-movie.component.html',
  styleUrls: ['return-movie.component.css']
})
export class ReturnMovieComponent {
  @Input() rent: Rent;
  currentRating: number = 0;

  constructor(private _moviesService: MoviesService,
    private _rentService: RentService) { }

  calculateAmount(rentedMovie: Rent): number {
    return this._rentService.calculatePay(rentedMovie);
  }

  returnMovie(rentedMovie: Rent): void {
    this._moviesService.fbChangeMovieStatusToFree(rentedMovie.movieId);
    this._moviesService.fbAddRating(rentedMovie.movieId, this.currentRating);
    this._rentService.fbReturnTape(rentedMovie);
  }
}
