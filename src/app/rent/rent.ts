export class Rent {
    id: number;
    movieId:number;
    movieTitle: string;
    price: number;
    borrower: string;
    lendDate: string;
    returnDate: string;
    daysCount: number;
    unrewindedTape: boolean;
    delay: number;
    brokenTape: boolean;
    pay: number;
}
