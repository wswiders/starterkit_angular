import {RentService} from './rent.service';
import {Rent} from './rent';

describe('Rent Service', () => {

  let service: RentService;
  beforeEach(() => {
    service = new RentService();
  });

  describe('calculating pay method', () => {
    let rentedMovie: Rent;
    beforeEach(() => {
      rentedMovie = new Rent();
      rentedMovie.lendDate = '2017-04-04';
      rentedMovie.returnDate = '2017-04-07';
      rentedMovie.price = 10;
    });

    it('should return 30 when tape was rent for 3 days', () => {
      //when
      let actual = service.calculatePay(rentedMovie);
      //then
      expect(actual).toBe(30);
    });

    it('should return 140 when tape was rent for 14 days', () => {
      //given
      rentedMovie.returnDate = '2017-04-18';
      //when
      let actual = service.calculatePay(rentedMovie);
      //then
      expect(actual).toBe(140);
    });

    it('should return 164 when tape was rent for 16 days', () => {
      //given
      rentedMovie.returnDate = '2017-04-20';
      //when
      let actual = service.calculatePay(rentedMovie);
      //then
      expect(actual).toBe(164);
    });

    it('should return 40 when tape was rent for 3 days and it is brokent', () => {
      //given
      rentedMovie.brokenTape = true;
      //when
      let actual = service.calculatePay(rentedMovie);
      //then
      expect(actual).toBe(40);
    });

    it('should return 40 when tape was rent for 3 days and it is unrewinded', () => {
      //given
      rentedMovie.unrewindedTape = true;
      //when
      let actual = service.calculatePay(rentedMovie);
      //then
      expect(actual).toBe(40);
    });

    it('should return 50 when tape was rent for 3 days and it is unrewinded and broken', () => {
      //given
      rentedMovie.unrewindedTape = true;
      rentedMovie.brokenTape = true;
      //when
      let actual = service.calculatePay(rentedMovie);
      //then
      expect(actual).toBe(50);
    });
  });
});
