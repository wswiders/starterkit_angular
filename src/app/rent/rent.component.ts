import {Component, OnInit} from '@angular/core';

import {Rent} from './rent';
import { RentService } from './rent.service';

@Component({
  moduleId: module.id,
  templateUrl: 'rent.component.html',
  styleUrls: ['rent.component.css']
})
export class RentComponent implements OnInit {
  pageTitle: string = 'Rent Move List';
  selectedRent: Rent;
  copiedRent: Rent = new Rent();
  movieTitleRentFilter: string;
  borrowerFilter: string;
  orderBy: string = 'returnDate';
  asc: boolean = true;

  constructor(private _rentService: RentService) { }

  ngOnInit(): void {
    this._rentService.fbGetListOfRentedMovie();
  }

  selectRentedMovie(rent: Rent) {
    this.selectedRent = rent;
    this.copyRent(rent);
  }

  setSortParams(field: string, asc: boolean) {
    this.orderBy = field;
    this.asc = asc;
  }

  private copyRent(rent: Rent): void{
    for(var k in rent){
      this.copiedRent[k]=rent[k];
    }
  }
}
