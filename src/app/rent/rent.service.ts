import { Injectable } from '@angular/core';

import {Rent} from './rent';

declare var firebase: any;
const dataRef: string = '/Rent/';
const maxRentPeriod: number = 14;
const unrewindedTapeCost: number = 10;
const brokenTapeCost: number = 10;
const delayCost: number = 2;

@Injectable()
export class RentService {
  rents: Rent[] = [];

  fbGetListOfRentedMovie(): void {
    this.rents = [];
    firebase.database().ref(dataRef).on('child_added', (snapshot: any) => {
      this.rents.push(snapshot.val())
    });
  }

  fbAddNewRent(rent: Rent): void {
    let nextId = this.fbGetNextId();
    firebase.database().ref(dataRef + nextId).set({
      id: nextId,
      movieId: rent.movieId,
      movieTitle: rent.movieTitle,
      price: rent.price,
      borrower: rent.borrower,
      lendDate: rent.lendDate,
      daysCount: 0,
      returnDate: '',
      unrewindedTape: '',
      delay: '',
      brokenTape: '',
      pay: ''
    });
  }

  fbReturnTape(rent: Rent): void {
    firebase.database().ref(dataRef + rent.id).update({
      returnDate: rent.returnDate,
      daysCount: rent.daysCount,
      unrewindedTape: rent.unrewindedTape,
      delay: rent.delay,
      brokenTape: rent.brokenTape,
      pay: this.calculatePay(rent)
    });
  }

  calculatePay(rentedMovie: Rent): number {
    this.calculateDateData(rentedMovie);
    let pay = 0;
    if (rentedMovie.unrewindedTape) {
      pay += unrewindedTapeCost;
    }
    if (rentedMovie.brokenTape) {
      pay += brokenTapeCost;
    }
    if (rentedMovie.delay != 0) {
      pay += rentedMovie.delay * delayCost;
    }
    pay += rentedMovie.daysCount * rentedMovie.price;
    return pay;
  }

  private calculateDateData(rentMovie: Rent) {
    rentMovie.daysCount = this.calculatePeriodInDays(rentMovie);
    rentMovie.delay = this.calculateDelay(rentMovie);
  }

  private calculatePeriodInDays(rentedMovie: Rent) {
    if(rentedMovie.returnDate){
      let startDate: Date = new Date(rentedMovie.lendDate);
      let endDate: Date = new Date(rentedMovie.returnDate);
      let timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
      return Math.ceil(timeDiff / (1000 * 3600 * 24));
    }
    return 0;
  }

  private calculateDelay(rentedMovie: Rent) {
    if (rentedMovie.daysCount > maxRentPeriod) {
      return rentedMovie.daysCount - maxRentPeriod;
    }
    return 0;
  }

  private fbGetNextId(): number {
    this.fbGetListOfRentedMovie();
    if (this.rents.length != 0) {
      return this.rents.length;
    }
    return 0;
  }

}
