import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';

import { MoviesService } from './movies.service';
import { RentService } from '../rent/rent.service';

import { MoviesComponent } from './movies.component';
import { MovieDetailsComponent } from './details/movie-details.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { EditMovieComponent } from './edit-movie/edit-movie.component';
import { RentMovieComponent } from './rent-movie/rent-movie.component';

import { MoviesFilterPipe } from './movies-filter.pipe';
import { MoviesSortPipe } from './movies-sort.pipe';

import { RatingModule } from "ngx-rating";

import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      { path: 'movies', component: MoviesComponent }
    ]),
    RatingModule
  ],
  declarations: [
    MoviesComponent,
    MovieDetailsComponent,
    AddMovieComponent,
    EditMovieComponent,
    RentMovieComponent,
    MoviesFilterPipe,
    MoviesSortPipe],
  providers: [MoviesService, RentService]
})
export class MoviesModule { }
