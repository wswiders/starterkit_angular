import {Component, OnInit} from '@angular/core';

import {Movie} from './movie';
import { MoviesService } from './movies.service';

@Component({
  moduleId: module.id,
  templateUrl: 'movies.component.html',
  styleUrls: ['movies.component.css']
})
export class MoviesComponent implements OnInit {
  pageTitle: string = 'Movies List';
  selectedMovie: Movie;
  copiedMovie: Movie = new Movie();
  //filters
  movieTitleFilter: string;
  movieGenreFilter: string;
  //sorted
  orderBy: string = 'id';
  asc: boolean = true;

  constructor(private _moviesService: MoviesService) { }

  ngOnInit(): void {
    this._moviesService.fbGetMovies();
  }

  setSelectedMovie(movie: Movie) {
    this.selectedMovie = movie;
    this.copyMovie(movie);
  }

  setSortParams(field: string, asc: boolean) {
    this.orderBy = field;
    this.asc = asc;
  }

  private copyMovie(movie: Movie): void{
    for(var k in movie){
      this.copiedMovie[k]=movie[k];
    }
  }
}
