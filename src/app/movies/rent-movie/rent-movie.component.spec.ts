import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA }    from '@angular/core';

import { SharedModule } from '../../shared/shared.module';

import {RentMovieComponent} from './rent-movie.component';
import { MoviesService } from '../movies.service';
import { RentService } from '../../rent/rent.service';
import { Movie } from '../movie';
import { Rent } from '../../rent/rent';

describe('Rent Movie Component', () => {

  let comp: RentMovieComponent;
  let fixture: ComponentFixture<RentMovieComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    let movkMoviesService = {
      fbChangeMovieStatusToRent: () => true,
      fbGetMovies: () => true
    };
    let mockRentService = {
      fbAddNewRent: () => true
    };
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [RentMovieComponent],
      providers: [
        { provide: MoviesService, useValue: movkMoviesService },
        { provide: RentService, useValue: mockRentService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentMovieComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
    el = fixture.nativeElement;

    //given
    comp.movie = new Movie();
    comp.movie.id = 1;
    comp.movie.title = 'test title';
    comp.movie.price = 10;
    fixture.detectChanges();
  });

  describe('init page', ()=>{

    it('should display empty input form on modal window start', async(() => {
      //when
      fixture.whenStable().then(() =>{
        fixture.detectChanges();
        //then
        expect(<HTMLInputElement>de.query(By.css("#borrower")).nativeElement.value).toBe("");
        expect(<HTMLInputElement>de.query(By.css("#lendDate")).nativeElement.value).toBe("");
      });
    }));
  });

  describe('page events', ()=>{

    it('should rent button be disabled on modal window start', async(() => {
      //when
      fixture.whenStable().then(() =>{
        fixture.detectChanges();
        let btn = <HTMLButtonElement>de.query(By.css('#rentMovieBtn')).nativeElement;
        //then
        expect(btn.disabled).toBe(true);
      });
    }));

    it('should rent button be disabled when input is invalid', async(() => {
      //when
      comp.rent.borrower = 'test borrower';
      fixture.detectChanges();
      fixture.whenStable().then(() =>{
        fixture.detectChanges();
        let btn = <HTMLButtonElement>de.query(By.css('#rentMovieBtn')).nativeElement;
        //then
        expect(btn.disabled).toBe(true);
      });
    }));

    it('should rent button be available when input is valid', async(() => {
      //when
      comp.rent.borrower = 'test borrower';
      comp.rent.lendDate = '2017-04-04';
      fixture.detectChanges();
      fixture.whenStable().then(() =>{
        fixture.detectChanges();
        let btn = <HTMLButtonElement>de.query(By.css('#rentMovieBtn')).nativeElement;
        //then
        expect(btn.disabled).toBe(false);
      });
    }));

    it('should call rentMovie method when click rent button', async(() => {
      let spy = spyOn(comp, 'rentMovie');
      //when
      comp.rent.borrower = 'test borrower';
      comp.rent.lendDate = '2017-04-04';
      fixture.detectChanges();
      fixture.whenStable().then(() =>{
        fixture.detectChanges();
        let btn = <HTMLButtonElement>de.query(By.css('#rentMovieBtn')).nativeElement;
        btn.click();
        //then
        expect(spy).toHaveBeenCalled();
      });
    }));
  });

});
