import {Component, OnInit, Input} from '@angular/core';

import {Movie} from '../movie';
import {Rent} from '../../rent/rent';

import { MoviesService } from '../movies.service';
import { RentService } from '../../rent/rent.service';

@Component({
  moduleId: module.id,
  selector: 'rent-movie',
  templateUrl: 'rent-movie.component.html',
  styleUrls: ['rent-movie.component.css']
})
export class RentMovieComponent implements OnInit {
  @Input() movie: Movie;
  rent: Rent;

  constructor(private _moviesService: MoviesService,
              private _rentService: RentService) { }

  ngOnInit(): void {
    this.rent = new Rent();
  }

  rentMovie() {
    this.rent.movieId = this.movie.id;
    this.rent.movieTitle = this.movie.title;
    this.rent.price = this.movie.price;
    this._moviesService.fbChangeMovieStatusToRent(this.movie.id);
    this._rentService.fbAddNewRent(this.rent);
  }

}
