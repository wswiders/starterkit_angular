import { Pipe, PipeTransform } from "@angular/core";

import { Movie } from './movie';

@Pipe({
  name: 'moviesFilter'
})
export class MoviesFilterPipe implements PipeTransform {

  transform(value: Movie[], filterByTitle: string, filterByGenre: string): Movie[] {
    filterByTitle = filterByTitle ? filterByTitle.toLocaleLowerCase() : null;
    filterByGenre = filterByGenre != "" ? filterByGenre : null;
    if (filterByGenre && filterByTitle) {
      return value.filter((movie: Movie) => (movie.genre == filterByGenre && movie.title.toLocaleLowerCase().indexOf(filterByTitle) !== -1));
    }
    if (!filterByGenre && filterByTitle) {
      return value.filter((movie: Movie) => movie.title.toLocaleLowerCase().indexOf(filterByTitle) !== -1);
    }
    if (filterByGenre && !filterByTitle) {
      return value.filter((movie: Movie) => movie.genre == filterByGenre);
    }
    if (!filterByGenre && !filterByTitle) {
      return value;
    }
  }
}
