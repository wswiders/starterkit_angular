import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA }    from '@angular/core';

import { SharedModule } from '../../shared/shared.module';

import {AddMovieComponent} from './add-movie.component';
import { MoviesService } from '../movies.service';
import { Movie } from '../movie';

describe('Add Movie Component', () => {

  let comp: AddMovieComponent;
  let fixture: ComponentFixture<AddMovieComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    let movkMoviesService = {
      fbAddNewMovie: () => true,
      fbGetMovies: () => true
    };
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [AddMovieComponent],
      providers: [
        { provide: MoviesService, useValue: movkMoviesService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMovieComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
    el = fixture.nativeElement;
  });

  describe('init display', () => {

    it('should display empty input form when open modal window', async(() => {
      //given
      comp.ngOnInit();
      fixture.detectChanges();
      //when
      fixture.whenStable().then(() => {
        expect(<HTMLInputElement>de.query(By.css('#title')).nativeElement.value).toBe('')
        expect(<HTMLInputElement>de.query(By.css('#releasedYear')).nativeElement.value).toBe('')
        expect(<HTMLInputElement>de.query(By.css('#price')).nativeElement.value).toBe('')
        expect(<HTMLInputElement>de.query(By.css('#genre')).nativeElement.value).toBe('')
      });
    }));
  });

  describe('page events', () => {

    it('should saveMovie button be disabled when not all require data are insert', async(() => {
      //given
      comp.ngOnInit();
      fixture.detectChanges();
      //when
      comp.movie.title = 'test title';
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        //then
        fixture.detectChanges();
        expect(<HTMLButtonElement>de.query(By.css('#saveMovieBtn')).nativeElement.disabled).toBe(true);
      });
    }));

    it('should saveMovie button be available when all require data are insert', async(() => {
      //given
      comp.ngOnInit();
      fixture.detectChanges();
      //when
      comp.movie.title = 'test title';
      comp.movie.director = 'test director';
      comp.movie.genre = 'Horror';
      comp.movie.releasedDate = '2017-03-11';
      comp.movie.price = 2;
      comp.movie.cover = 'http://samequizy.pl/wp-content/uploads/2016/02/filing_images_cdf9256fb788.jpg';
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        //then
        fixture.detectChanges();
        expect(<HTMLButtonElement>de.query(By.css('#saveMovieBtn')).nativeElement.disabled).toBe(false);
      });
    }));

    it('should saveMovie button be disabled when any input is invalid', async(() => {
      //given
      comp.ngOnInit();
      fixture.detectChanges();
      //when
      comp.movie.title = 'test title';
      comp.movie.director = 'test director';
      comp.movie.genre = 'Horror';
      comp.movie.releasedDate = '2017-03-11';
      comp.movie.price = 2;
      comp.movie.cover = 'http://samequizy.pl/wp-content/uploads/2016/02/filing_images_cdf9256fb788.jpg';
      comp.movie.movieGif = 'dsadsdasa'
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        //then
        fixture.detectChanges();
        expect(<HTMLButtonElement>de.query(By.css('#saveMovieBtn')).nativeElement.disabled).toBe(true);
      });
    }));

    it('should call saveMovie method on button click', async(() => {
      //given
      let spy = spyOn(comp, 'saveMovie')
      comp.ngOnInit();
      fixture.detectChanges();
      //when
      comp.movie.title = 'test title';
      comp.movie.director = 'test director';
      comp.movie.genre = 'Horror';
      comp.movie.releasedDate = '2017-03-11';
      comp.movie.price = 2;
      comp.movie.cover = 'http://samequizy.pl/wp-content/uploads/2016/02/filing_images_cdf9256fb788.jpg';
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        let btn = <HTMLButtonElement>de.query(By.css('#saveMovieBtn')).nativeElement;
        btn.click();
        //then
        expect(spy).toHaveBeenCalled();
      });
    }));
  });
});
