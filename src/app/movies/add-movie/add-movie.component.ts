import {Component, OnInit} from '@angular/core';

import {Movie} from '../movie';

import { MoviesService } from '../movies.service';

@Component({
  moduleId: module.id,
  selector: 'add-movie',
  templateUrl: 'add-movie.component.html',
  styleUrls: ['add-movie.component.css']
})
export class AddMovieComponent implements OnInit{
  movie: Movie;

  constructor(private _moviesService: MoviesService) {}

  ngOnInit(): void{
    this.movie = new Movie();
  }

  saveMovie(){
    this._moviesService.fbAddNewMovie(this.movie);
  }
}
