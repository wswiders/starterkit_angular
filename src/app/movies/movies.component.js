"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var movie_1 = require("./movie");
var movies_service_1 = require("./movies.service");
var MoviesComponent = (function () {
    function MoviesComponent(_moviesService) {
        this._moviesService = _moviesService;
        this.pageTitle = 'Movies List';
        this.copiedMovie = new movie_1.Movie();
        //sorted
        this.orderBy = 'id';
        this.asc = true;
    }
    MoviesComponent.prototype.ngOnInit = function () {
        this._moviesService.fbGetMovies();
    };
    MoviesComponent.prototype.setSelectedMovie = function (movie) {
        this.selectedMovie = movie;
        this.copyMovie(movie);
    };
    MoviesComponent.prototype.setSortParams = function (field, asc) {
        this.orderBy = field;
        this.asc = asc;
    };
    MoviesComponent.prototype.copyMovie = function (movie) {
        for (var k in movie) {
            this.copiedMovie[k] = movie[k];
        }
    };
    return MoviesComponent;
}());
MoviesComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'movies.component.html',
        styleUrls: ['movies.component.css']
    }),
    __metadata("design:paramtypes", [movies_service_1.MoviesService])
], MoviesComponent);
exports.MoviesComponent = MoviesComponent;
//# sourceMappingURL=movies.component.js.map