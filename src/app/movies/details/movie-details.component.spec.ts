import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA }    from '@angular/core';

import {MovieDetailsComponent} from './movie-details.component';
import {Movie} from '../movie';

describe('Movie Detail Component', () => {

  let comp: MovieDetailsComponent;
  let fixture: ComponentFixture<MovieDetailsComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieDetailsComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailsComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
    el = fixture.nativeElement;
  });

  it('should set correct data on modal window', () => {
    //given
    let movie: Movie = new Movie();
    movie.id = 0;
    movie.title = "Bułgarski pościkk";
    movie.director = "Bartosz Walaszek";
    movie.releasedDate = "2001-01-01";
    movie.genre = "Comedy";
    movie.cover = "https://i.ytimg.com/vi/faDEJq4pG5s/hqdefault.jpg";
    movie.movieGif = "https://media1.giphy.com/media/xUA7bimNpvuSyHdh0Q/200.webp#5";
    movie.price = 9.5;
    movie.status = "FREE";
    movie.rating = 4.9;
    //when
    comp.movie = movie;
    fixture.detectChanges();
    //then
    expect(el.querySelector('.info').textContent).toContain('Title: Bułgarski pościkk');
    expect(el.querySelector('.info').textContent).toContain('Director: Bartosz Walaszek');
    expect(el.querySelector('.info').textContent).toContain('Release date: Jan 1, 2001');
  });
});
