import {Component, Input} from '@angular/core';

import {Movie} from '../movie';

@Component({
  moduleId: module.id,
  selector: 'app-details',
  templateUrl: 'movie-details.component.html',
  styleUrls: ['movie-details.component.css']
})
export class MovieDetailsComponent{
  @Input() movie: Movie;
}
