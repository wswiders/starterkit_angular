"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var movies_service_1 = require("./movies.service");
var rent_service_1 = require("../rent/rent.service");
var movies_component_1 = require("./movies.component");
var movie_details_component_1 = require("./details/movie-details.component");
var add_movie_component_1 = require("./add-movie/add-movie.component");
var edit_movie_component_1 = require("./edit-movie/edit-movie.component");
var rent_movie_component_1 = require("./rent-movie/rent-movie.component");
var movies_filter_pipe_1 = require("./movies-filter.pipe");
var movies_sort_pipe_1 = require("./movies-sort.pipe");
var ngx_rating_1 = require("ngx-rating");
var shared_module_1 = require("../shared/shared.module");
var MoviesModule = (function () {
    function MoviesModule() {
    }
    return MoviesModule;
}());
MoviesModule = __decorate([
    core_1.NgModule({
        imports: [
            shared_module_1.SharedModule,
            router_1.RouterModule.forChild([
                { path: 'movies', component: movies_component_1.MoviesComponent }
            ]),
            ngx_rating_1.RatingModule
        ],
        declarations: [
            movies_component_1.MoviesComponent,
            movie_details_component_1.MovieDetailsComponent,
            add_movie_component_1.AddMovieComponent,
            edit_movie_component_1.EditMovieComponent,
            rent_movie_component_1.RentMovieComponent,
            movies_filter_pipe_1.MoviesFilterPipe,
            movies_sort_pipe_1.MoviesSortPipe
        ],
        providers: [movies_service_1.MoviesService, rent_service_1.RentService]
    })
], MoviesModule);
exports.MoviesModule = MoviesModule;
//# sourceMappingURL=movies.module.js.map