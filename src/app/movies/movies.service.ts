import { Injectable, OnInit } from '@angular/core';

import {Movie} from './movie';

declare var firebase: any;
const dataRef: string = '/Movies/';

@Injectable()
export class MoviesService {
  movies: Movie[] = [];

  fbGetMovies(): void {
    this.movies = [];
    firebase.database().ref(dataRef).on('child_added', (snapshot: any) => {
      this.movies.push(snapshot.val())
    });
  }

  fbAddNewMovie(movie: Movie): void {
    let nextId = this.fbGetNextId() + 1;
    firebase.database().ref(dataRef + nextId).set({
      id: nextId,
      title: movie.title,
      director: movie.director,
      releasedDate: movie.releasedDate,
      genre: movie.genre,
      price: movie.price,
      cover: movie.cover,
      movieGif: movie.movieGif,
      status: "FREE",
      rating: 0,
      ratingSum: 0,
      ratingCount: 0
    });
  }

  fbUpdateMovie(movie: Movie): void {
    firebase.database().ref(dataRef + movie.id).update({
      title: movie.title,
      director: movie.director,
      releasedDate: movie.releasedDate,
      genre: movie.genre,
      price: movie.price,
      cover: movie.cover,
      movieGif: movie.movieGif
    });
  }

  fbChangeMovieStatusToRent(movieId: number): void {
    firebase.database().ref(dataRef + movieId).update({ status: "RENT" });
  }

  fbChangeMovieStatusToFree(movieId: number): void {
    firebase.database().ref(dataRef + movieId).update({ status: "FREE" });
  }

  fbAddRating(movieId: number, rating: number): void {
    if(rating !==0){
      let movie = this.findMovie(movieId);
      let newRatingSum = movie.ratingSum + rating;
      let newRatingCount = movie.ratingCount + 1;
      let newRating = newRatingSum/newRatingCount;
      firebase.database().ref(dataRef + movieId).update({
        rating: newRating,
        ratingSum: newRatingSum,
        ratingCount: newRatingCount
      });
    }
  }

  private findMovie(movieId: number): Movie {
    for (let movie of this.movies) {
      if (movie.id === movieId) {
        return movie;
      }
    }
  }

  private fbGetNextId(): number {
    this.fbGetMovies();
    if (this.movies.length != 0) {
      return this.movies[this.movies.length - 1].id;
    }
    return 0;
  }
}
