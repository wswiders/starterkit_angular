"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var dataRef = '/Movies/';
var MoviesService = (function () {
    function MoviesService() {
        this.movies = [];
    }
    MoviesService.prototype.fbGetMovies = function () {
        var _this = this;
        this.movies = [];
        firebase.database().ref(dataRef).on('child_added', function (snapshot) {
            _this.movies.push(snapshot.val());
        });
    };
    MoviesService.prototype.fbAddNewMovie = function (movie) {
        var nextId = this.fbGetNextId() + 1;
        firebase.database().ref(dataRef + nextId).set({
            id: nextId,
            title: movie.title,
            director: movie.director,
            releasedDate: movie.releasedDate,
            genre: movie.genre,
            price: movie.price,
            cover: movie.cover,
            movieGif: movie.movieGif,
            status: "FREE",
            rating: 0,
            ratingSum: 0,
            ratingCount: 0
        });
    };
    MoviesService.prototype.fbUpdateMovie = function (movie) {
        firebase.database().ref(dataRef + movie.id).update({
            title: movie.title,
            director: movie.director,
            releasedDate: movie.releasedDate,
            genre: movie.genre,
            price: movie.price,
            cover: movie.cover,
            movieGif: movie.movieGif
        });
    };
    MoviesService.prototype.fbChangeMovieStatusToRent = function (movieId) {
        firebase.database().ref(dataRef + movieId).update({ status: "RENT" });
    };
    MoviesService.prototype.fbChangeMovieStatusToFree = function (movieId) {
        firebase.database().ref(dataRef + movieId).update({ status: "FREE" });
    };
    MoviesService.prototype.fbAddRating = function (movieId, rating) {
        if (rating !== 0) {
            var movie = this.findMovie(movieId);
            var newRatingSum = movie.ratingSum + rating;
            var newRatingCount = movie.ratingCount + 1;
            var newRating = newRatingSum / newRatingCount;
            firebase.database().ref(dataRef + movieId).update({
                rating: newRating,
                ratingSum: newRatingSum,
                ratingCount: newRatingCount
            });
        }
    };
    MoviesService.prototype.findMovie = function (movieId) {
        for (var _i = 0, _a = this.movies; _i < _a.length; _i++) {
            var movie = _a[_i];
            if (movie.id === movieId) {
                return movie;
            }
        }
    };
    MoviesService.prototype.fbGetNextId = function () {
        this.fbGetMovies();
        if (this.movies.length != 0) {
            return this.movies[this.movies.length - 1].id;
        }
        return 0;
    };
    return MoviesService;
}());
MoviesService = __decorate([
    core_1.Injectable()
], MoviesService);
exports.MoviesService = MoviesService;
//# sourceMappingURL=movies.service.js.map