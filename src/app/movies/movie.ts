export class Movie {
    public id: number;
    public title: string;
    public director: string;
    public releasedDate: string;
    public genre: string;
    public cover: string;
    public movieGif: string = '';
    public price: number;
    public status: string;
    public rating: number;
    public ratingSum: number;
    public ratingCount: number;
}
