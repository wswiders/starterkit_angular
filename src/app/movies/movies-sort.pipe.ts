import { Pipe, PipeTransform } from '@angular/core';

import { Movie } from './movie';

@Pipe({
  name: 'sortMovies'
})
export class MoviesSortPipe implements PipeTransform {

  transform(value: Movie[], sortedParam: string, asc: boolean): Movie[] {
    value.sort((a, b) => {
      if (a[sortedParam] < b[sortedParam]) {
        return -1;
      } else if (a[sortedParam] > b[sortedParam]) {
        return 1;
      } else {
        return 0;
      }
    });
    if (asc) {
      return value;
    } else {
      return value.reverse();
    }
  }
}
