import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA }    from '@angular/core';

import { MoviesFilterPipe } from './movies-filter.pipe';
import { MoviesSortPipe } from './movies-sort.pipe';

import {Movie} from './movie';
import { MoviesService } from './movies.service';
import { MoviesComponent } from './movies.component';

describe('Movies Component', () => {

  let comp: MoviesComponent;
  let fixture: ComponentFixture<MoviesComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    let mockMoviesService = {
      movies: [{
        "cover": "http://1.fwcdn.pl/po/18/61/311861/7710200.3.jpg",
        "director": "Bartosz Walaszek",
        "genre": "Comedy",
        "id": 2,
        "movieGif": "https://media1.giphy.com/media/xUA7bimNpvuSyHdh0Q/200.webp#5",
        "price": 9.5,
        "rating": 4.9,
        "releasedDate": "2006-01-01",
        "status": "FREE",
        "title": "Wściekłe pięści Węża"
      }, {
          "cover": "https://i.ytimg.com/vi/faDEJq4pG5s/hqdefault.jpg",
          "director": "Bartosz Walaszek",
          "genre": "Horror",
          "id": 1,
          "movieGif": "https://media1.giphy.com/media/xUA7bimNpvuSyHdh0Q/200.webp#5",
          "price": 9.5,
          "rating": 4.9,
          "releasedDate": "2001-01-01",
          "status": "FREE",
          "title": "Bułgarski pościkk"
        }],
      fbGetMovies: () => true
    }
    TestBed.configureTestingModule({
      declarations: [MoviesComponent, MoviesFilterPipe, MoviesSortPipe],
      providers: [
        { provide: MoviesService, useValue: mockMoviesService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
    el = fixture.nativeElement;
  });

  describe('init display', () => {

    it('should set correct title', () => {
      // when
      comp.ngOnInit();
      fixture.detectChanges();
      // then
      expect(el.querySelector('.panel-heading').textContent).toContain('Movies List');
    });

    it('should set elements in table in default correct order', () => {
      //when
      comp.ngOnInit();
      fixture.detectChanges();
      //then
      expect(el.querySelector('[t1]').textContent).toContain("Bułgarski pościkk");
    });
  });

  describe('display after modification', () => {

    it('should set elements in table in correct order ascending when orderBy is genre', () => {
      // given
      comp.orderBy = 'genre';
      comp.asc = true;
      //when
      comp.ngOnInit();
      fixture.detectChanges();
      //then
      expect(el.querySelector('[t1]').textContent).toContain("Wściekłe pięści Węża");
    });

    it('should filter elements by movie title', () => {
      //given
      comp.movieTitleFilter = 'Bu'
      //when
      comp.ngOnInit();
      fixture.detectChanges();
      //then
      expect(el.querySelector('.table-responsive')).not.toContain("Wściekłe pięści Węża");
      expect(el.querySelector('[t1]').textContent).toContain("Bułgarski pościkk");
    });

    it('should filter elements by borrower name', () => {
      //given
      comp.movieGenreFilter = 'Horror'
      //when
      comp.ngOnInit();
      fixture.detectChanges();
      //then
      expect(el.querySelector('.table-responsive')).not.toContain("Wściekłe pięści Węża");
      expect(el.querySelector('[t1]').textContent).toContain("Bułgarski pościkk");
    });
  });

  describe('page event', () => {

    it('should call selectRentedMovie method when press edit button', () => {
      // given
      let spy = spyOn(comp, 'setSelectedMovie');
      comp.ngOnInit();
      fixture.detectChanges();
      let btn = de.query(By.css('#editBtn')).nativeElement;
      //when
      btn.click();
      // then
      expect(spy).toHaveBeenCalled();
    });

    it('should call setSortParams method when press button', () => {
      // given
      let spy = spyOn(comp, 'setSortParams');
      comp.ngOnInit();
      fixture.detectChanges();
      let btn = de.query(By.css('#titleAsc')).nativeElement;
      //when
      btn.click();
      // then
      expect(spy).toHaveBeenCalledWith('title', true);
    });
  });

});
