import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA }    from '@angular/core';

import { SharedModule } from '../../shared/shared.module';

import {EditMovieComponent} from './edit-movie.component';
import { MoviesService } from '../movies.service';
import { Movie } from '../movie';

describe('Edit Movie Component', () => {

  let comp: EditMovieComponent;
  let fixture: ComponentFixture<EditMovieComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    let movkMoviesService = {
      fbUpdateMovie: () => true,
      fbGetMovies: () => true
    };
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [EditMovieComponent],
      providers: [
        { provide: MoviesService, useValue: movkMoviesService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMovieComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
    el = fixture.nativeElement;

    //given
    comp.editingMovie = new Movie();
    comp.editingMovie.title = 'test title';
    comp.editingMovie.director = 'test director';
    comp.editingMovie.genre = 'Horror';
    comp.editingMovie.releasedDate = '2017-03-11';
    comp.editingMovie.price = 2;
    comp.editingMovie.status = 'FREE';
    comp.editingMovie.cover = 'http://samequizy.pl/wp-content/uploads/2016/02/filing_images_cdf9256fb788.jpg';
    comp.editingMovie.movieGif = 'http://samequizy.pl/wp-content/uploads/2016/02/filing_images_cdf9256fb788.jpg';
    fixture.detectChanges();
  });

  describe('init display', () => {

    it('should display full input form when movie status is free', async(() => {
      //when
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        //then
        expect(<HTMLInputElement>de.query(By.css('#title')).nativeElement.value).toBe('test title')
        expect(<HTMLInputElement>de.query(By.css('#releasedYear')).nativeElement.value).toBe('2017-03-11')
        expect(<HTMLInputElement>de.query(By.css('#price')).nativeElement.value).toBe('2')
        expect(<HTMLInputElement>de.query(By.css('#genre')).nativeElement.value).toBe('Horror')
      });
    }));

    it('should not display title and price input form when movie status is free', async(() => {
      //given
      comp.editingMovie.status = 'RENT';
      fixture.detectChanges();
      //when
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        //then
        expect(de.query(By.css('#title'))).toBeNull();
        expect(de.query(By.css('#price'))).toBeNull();
      });
    }));
  });

  describe('page events', () => {

    it('should saveMovie button be disabled when input is invalid', async(() => {
      //when
      comp.editingMovie.title = '';
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        //then
        fixture.detectChanges();
        expect(<HTMLButtonElement>de.query(By.css('#updateMovieBtn')).nativeElement.disabled).toBe(true);
      });
    }));

    it('should saveMovie button be available when all require data are valid', async(() => {
      //when
      fixture.whenStable().then(() => {
        //then
        fixture.detectChanges();
        expect(<HTMLButtonElement>de.query(By.css('#updateMovieBtn')).nativeElement.disabled).toBe(false);
      });
    }));

    it('should saveMovie button be available when input is invalid', async(() => {
      //when
      comp.editingMovie.title = 'test title2';
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        //then
        fixture.detectChanges();
        expect(<HTMLButtonElement>de.query(By.css('#updateMovieBtn')).nativeElement.disabled).toBe(false);
      });
    }));

    it('should call updateMovie method on button click', async(() => {
      //given
      let spy = spyOn(comp, 'updateMovie')
      //when
      comp.editingMovie.title = 'test title2';
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        let btn = <HTMLButtonElement>de.query(By.css('#updateMovieBtn')).nativeElement;
        btn.click();
        //then
        expect(spy).toHaveBeenCalled();
      });
    }));
  });
});
