import {Component, Input} from '@angular/core';

import {Movie} from '../movie';

import { MoviesService } from '../movies.service';

@Component({
  moduleId: module.id,
  selector: 'edit-movie',
  templateUrl: 'edit-movie.component.html',
  styleUrls: ['edit-movie.component.css']
})
export class EditMovieComponent{
  @Input() editingMovie: Movie;

  constructor(private _moviesService: MoviesService) { }

  updateMovie(movie: Movie): void {
    this._moviesService.fbUpdateMovie(movie);
  }
}
