import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import {AppComponent} from './app.component';

describe('App Component', () =>{

  let comp: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(() =>{
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(AppComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
    el = fixture.nativeElement;
  });

  it('should set correct page elements', () =>{
    //when
    fixture.detectChanges();
    //then
    expect(el.querySelector('[title]').textContent).toContain('VHS Rental Service');
    expect(el.querySelector('[movies]').textContent).toContain('Movies List');
    expect(el.querySelector('[rents]').textContent).toContain('Rented Movies List');
  });
});
