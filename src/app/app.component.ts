import { Component } from '@angular/core';

@Component({
    selector: 'pm-app',
    template: `
    <div>
        <nav class='navbar navbar-default'>
            <div class='container-fluid'>
                <a title class='navbar-brand'>{{pageTitle}}</a>
                <ul class='nav navbar-nav'>
                    <li><a movies [routerLink]="['/movies']">Movies List</a></li>
                    <li><a rents [routerLink]="['/rents']">Rented Movies List</a></li>
                </ul>
            </div>
        </nav>
        <div class="col-md-10 col-md-offset-1" >
            <router-outlet></router-outlet>
        </div>
     </div>
     `
})
export class AppComponent {
    pageTitle: string = 'VHS Rental Service';
}
